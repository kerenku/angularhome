// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCkF2DYVVFDDP8aOzc4GLJoAQ9cIaMgJcg",
    authDomain: "apphome-4bd82.firebaseapp.com",
    databaseURL: "https://apphome-4bd82.firebaseio.com",
    projectId: "apphome-4bd82",
    storageBucket: "apphome-4bd82.appspot.com",
    messagingSenderId: "1036861157912",
  }
  };


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
