import { Injectable } from '@angular/core';
import {Observable, observable} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {


  authors:any =  [{id:1, author:'Lewis Carrol'},{id:2, author:'Leo Tolstoy'},{id:3, author:'Thomas Mann'}];
  i:number =3;
  getAuthors(){
    const authorsObservable = new Observable(
      observer =>{
        setInterval (
          ()=> observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
  }
 // getBooks(){
  //  const booksObservable = new Observable(
  //  observer =>{
  //      setInterval (
  //        ()=> observer.next(this.books),5000
  //     )
  //    }
  //  )
  //  return booksObservable;
  //}
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges();
   }

  addAuthors(authornew:string){
    this.i=this.i+1;
    this.authors.push({id:this.i,author:authornew});
  }

  constructor(private db:AngularFirestore) { }
}


