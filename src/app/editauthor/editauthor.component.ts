import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  
 constructor(
            private router: Router,private route: ActivatedRoute) { }
  author:string;
  id;
  onSubmit(){
    this.router.navigate(['/Authors', this.id,this.author]);
  }
  ngOnInit() {
    this.id=this.route.snapshot.params.id;
  }

}