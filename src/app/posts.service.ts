import { Injectable } from '@angular/core';
import { HttpClient,HttpClientModule  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = "https://jsonplaceholder.typicode.com/posts/";
  apiUrlUsers="https://jsonplaceholder.typicode.com/users/";
  

  
  addPosts(name:string, body:string, author:string){
    const post = {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }

  constructor(private http:HttpClient,private db:AngularFirestore) { }
  
  
  // getPosts(){    
  //   return this.http.get<Posts[]>(this.apiUrl);
  // }

  // getUsers(){    
  //   return this.http.get<Users[]>(this.apiUrlUsers);
  // }
  
  //ex8
  // getPosts(): Observable<Posts>{
  //   return this.http.get<Posts>(this.apiUrl)
  //   }


  getPosts():Observable<any[]>{
    const ref = this.db.collection('posts');
    return ref.valueChanges({idField: 'id'});
  }

  
  getPost(id:string):Observable<any>{
    return this.db.doc(`posts/${id}`).get();
  }

  addPost(name:string, author:string, body:string){
    const post = {name:name, author:author, body:body}
    this.db.collection('posts').add(post);
  }
  deletePost(id:string){
    this.db.doc(`posts/${id}`).delete();
  }

  updatePost(id:string,name:string, author:string, body:string){
    this.db.doc(`posts/${id}`).update({
      name:name,
      author:author, 
      body:body
    })
  }
}




